Source: python-docutils
Section: python
Priority: optional
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Michael Schutte <michi@debian.org>,
           Dmitry Shachnev <mitya57@debian.org>
Build-Depends: debhelper (>= 9)
Build-Depends-Indep: dh-python,
                     python-all (>= 2.6.6-3~),
                     python-roman,
                     python3-all (>= 3.1.2-7~),
                     python3-distutils,
                     python3-roman,
                     xml-core
Standards-Version: 4.2.1
Vcs-Git: https://salsa.debian.org/python-team/modules/python-docutils.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/python-docutils
Homepage: http://docutils.sourceforge.net/

Package: python-docutils
Provides: docutils
Architecture: all
Multi-Arch: foreign
Depends: docutils-common (= ${source:Version}),
         python-roman,
         ${misc:Depends},
         ${python:Depends}
Recommends: docutils-doc (= ${source:Version}),
            libpaper-utils,
            python-pil,
            python-pygments
Suggests: fonts-linuxlibertine | ttf-linux-libertine,
          texlive-lang-french,
          texlive-latex-base,
          texlive-latex-recommended
Description: text processing system for reStructuredText (implemented in Python 2)
 reStructuredText is an easy-to-read, what-you-see-is-what-you-get plaintext
 markup syntax and parser system. It is useful for in-line program documentation
 (such as Python docstrings), for quickly creating simple web pages, and for
 standalone documents.
 .
 The purpose of the Docutils project is to create a set of tools for
 processing reStructuredText documentation into useful formats, such as HTML,
 LaTeX, ODT or Unix manpages.
 .
 This package includes Python 2 modules and command line utilities.

Package: python3-docutils
Provides: docutils
Architecture: all
Multi-Arch: foreign
Depends: docutils-common (= ${source:Version}),
         python3-roman,
         ${misc:Depends},
         ${python3:Depends}
Recommends: libpaper-utils, python3-pil, python3-pygments
Suggests: docutils-doc,
          fonts-linuxlibertine | ttf-linux-libertine,
          texlive-lang-french,
          texlive-latex-base,
          texlive-latex-recommended
Description: text processing system for reStructuredText (implemented in Python 3)
 reStructuredText is an easy-to-read, what-you-see-is-what-you-get plaintext
 markup syntax and parser system. It is useful for in-line program documentation
 (such as Python docstrings), for quickly creating simple web pages, and for
 standalone documents.
 .
 The purpose of the Docutils project is to create a set of tools for
 processing reStructuredText documentation into useful formats, such as HTML,
 LaTeX, ODT or Unix manpages.
 .
 This package includes Python 3 modules and command line utilities.

Package: docutils-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: python-docutils | python3-docutils
Description: text processing system for reStructuredText - common data
 reStructuredText is an easy-to-read, what-you-see-is-what-you-get plaintext
 markup syntax and parser system. It is useful for in-line program documentation
 (such as Python docstrings), for quickly creating simple web pages, and for
 standalone documents.
 .
 The purpose of the Docutils project is to create a set of tools for
 processing reStructuredText documentation into useful formats, such as HTML,
 LaTeX, ODT or Unix manpages.
 .
 This package includes data and configuration files.

Package: docutils-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: text processing system for reStructuredText - documentation
 reStructuredText is an easy-to-read, what-you-see-is-what-you-get plaintext
 markup syntax and parser system. It is useful for in-line program documentation
 (such as Python docstrings), for quickly creating simple web pages, and for
 standalone documents.
 .
 The purpose of the Docutils project is to create a set of tools for
 processing reStructuredText documentation into useful formats, such as HTML,
 LaTeX, ODT or Unix manpages.
 .
 This package includes documentation in HTML and (gzipped) reST formats.
